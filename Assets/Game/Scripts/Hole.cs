using UnityEngine;

namespace Assets.Game.Scripts
{
    public class Hole : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent<Ball>(out Ball ball))
            {
                Destroy(ball.gameObject);
            }
        }
    }
}