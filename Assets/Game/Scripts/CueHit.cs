﻿using UnityEngine;

namespace Assets.Game.Scripts
{
    public class CueHit : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _spriteRenderer;
        [SerializeField] private Rigidbody _ballRigidBody;
        [SerializeField] private float _hitForce;

        private Camera mainCamera;

        private void Start()
        {
            mainCamera = Camera.main;
        }

        private void Update()
        {
            if (_ballRigidBody == null) return;

            _spriteRenderer.enabled = _ballRigidBody.velocity.magnitude <= 0.01;

            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));

            Vector3 hitDirection = new Vector3(mousePosition.x, 0, mousePosition.z);
            
            if (Input.GetMouseButtonDown(0) && _spriteRenderer.enabled)
            {
                _ballRigidBody.AddForce(
                    (hitDirection - transform.position).normalized * _hitForce * Time.fixedDeltaTime, 
                    ForceMode.Impulse);
            }
        }
    }
}
