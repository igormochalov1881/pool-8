using UnityEngine;

namespace Assets.Game.Scripts
{
    public class CueRotation : MonoBehaviour
    {
        [SerializeField] private Transform _playerBallPosition;

        private Camera _camera;

        private void Start()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            if (_playerBallPosition == null) return;

            Vector3 ballPosition = new Vector3(_playerBallPosition.position.x, transform.position.y, _playerBallPosition.position.z);

            transform.position = ballPosition;

            Vector3 mousePosition = _camera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));

            float rotation = Quaternion.LookRotation(mousePosition - transform.position).eulerAngles.y;

            transform.rotation = Quaternion.Euler(0, rotation, 0);
        }
    }
}